#!/bin/bash

PROJECT_ROOT=$PWD

# generate result table
echo Generating the F1 Score and Resource Utilization Table ...
cd $PROJECT_ROOT && \
python collect-results.py --root $PROJECT_ROOT/deps
