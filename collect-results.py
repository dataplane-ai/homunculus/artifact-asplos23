# *************************************************************************
#
# Copyright 2023 Tushar Swamy (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

import csv
from tabulate import tabulate
import argparse


def collectResults(result_path):
    
    with open(result_path, newline='\n') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')

        result = None
        for row in reader:
            result = row            
        
        result.pop(1)

    return result


def formatResults(results):
    
    titles = ["Applications", "F1 Score", "CUs", "MUs"]

    baseline_anomaly_detection_results = ["Baseline Anomaly Detection", "71.10", "24", "48"]
    baseline_traffic_classification_results = ["Baseline Traffic Classification", "61.04", "31", "59"]
    baseline_botnet_detection_results = ["Baseline Botnet Detection", "77.04", "167", "45"]

    result_table = []

    result_table = [baseline_anomaly_detection_results,
                    results[0],
                    baseline_traffic_classification_results,
                    results[1],
                    baseline_botnet_detection_results,
                    results[2]]

    print(tabulate(result_table, headers=titles))


def main(root):
    
    results = []

    print("------Collecting Anomaly Detection Results------")
    result = collectResults(root + "/applications/anomaly-detection/main_plots/anomaly_detection_dnn.csv")
    result[0] = "Homunculus Anomaly Detection"
    results.append(result)
    print("-------------------------------------")

    print("------Collecting Traffic Classification Results------")
    result = collectResults(root + "/applications/traffic-classification/main_plots/traffic_classification_dnn.csv")
    result[0] = "Homunculus Traffic Classification"
    results.append(result)
    print("-------------------------------------")

    print("------Collecting Botnet Detection Results------")
    result = collectResults(root + "/applications/botnet-detection/main_plots/botnet_detection_dnn.csv")
    result[0] = "Homunculus Botnet Detection"
    results.append(result)
    print("-------------------------------------")

    
    formatResults(results)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--root", type=str, required=True)
    args = parser.parse_args()

    main(args.root)
