#!/bin/bash

# install linux dependencies
# sudo apt-get install -y python3.8-venv make

PROJECT_ROOT=$PWD

# setup compiler
cd $PROJECT_ROOT/deps/compiler/ && \
make build

# setup Taurus ASIC
cd $PROJECT_ROOT/deps/backends/taurus-asic
make deps
make