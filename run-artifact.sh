#!/bin/bash

PROJECT_ROOT=$PWD

# anomaly detection
echo Running Anomaly Detection Application ...
cd $PROJECT_ROOT/deps/applications/anomaly-detection && \
python main.py \
	--train-dataset $PROJECT_ROOT/deps/datasets/kdd-synthetic-traces/train_ad.csv \
	--test-dataset $PROJECT_ROOT/deps/datasets/kdd-synthetic-traces/test_ad.csv \
	--backend $PROJECT_ROOT/deps/backends/taurus-asic/ \
	--backend-type TaurusASIC

# traffic classification
echo Running Traffic Classification Application ...
cd $PROJECT_ROOT/deps/applications/traffic-classification && \
python main.py \
	--dataset $PROJECT_ROOT/deps/datasets/tmc-iot-traces/tmc-med.csv \
	--backend $PROJECT_ROOT/deps/backends/taurus-asic/ \
	--backend-type TaurusASIC

# botnet detection
echo Running Botnet Detection Application ...
cd $PROJECT_ROOT/deps/applications/botnet-detection && \
python main.py \
	--train-dataset $PROJECT_ROOT/deps/datasets/flowlens-flowmarkers-traces/Dataset_64_512.csv \
	--test-dataset $PROJECT_ROOT/deps/datasets/flowlens-flowmarkers-traces/PerPktHist_Subset10k_24k_64_512.csv \
	--backend $PROJECT_ROOT/deps/backends/taurus-asic \
	--backend-type TaurusASIC

# generate table
echo Generating the F1 Score and Resource Utilization Table ...
cd $PROJECT_ROOT && \
python collect-results.py --root $PROJECT_ROOT/deps
