# Homunculus Artifact for ASPLOS '23

In this repository, we share the source code for the various components of the Homunculus framework as described in the ASPLOS '23 paper.

- Compiler - https://gitlab.com/dataplane-ai/homunculus/compiler
- Backend (Taurus ASIC) - https://gitlab.com/dataplane-ai/homunculus/backends/taurus-asic
- Applications - https://gitlab.com/dataplane-ai/homunculus/applications
- Datasets - https://gitlab.com/dataplane-ai/homunculus/datasets

## Usage

- First, clone this repository and its submodules.
```
git clone https://gitlab.com/dataplane-ai/homunculus/artifact-asplos23.git
cd artifact-asplos23 && git submodule update --init --recursive
```

- Second, install the Linux dependencies and setup the Python virtual environment:
```sh
sudo apt-get install -y python3.8-venv make && \
python3 -m venv homunculus-venv && \
source homunculus-venv/bin/activate
```

- Third, if you are using `git` then Spatial uses some public packages from Github for which you need your ssh-keys already added to your Github account. To do so, you should follow the instructions given [here](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/adding-a-new-ssh-key-to-your-github-account).

- Fourth, build the Homunculus compiler and the Taurus ASIC backend by running the following command:
```sh
./setup-artifact.sh
```

- Fifth, run the applications (anomaly detection, traffic classification, botnet detection).
```sh
./run-artifact.sh
```

> **Note:** You can also execute it inside `tmux` by running `./run-artifact-with-tmux.sh`. To connect to `tmux` again, run `./connect-tmux.sh`.

- On successful completion, it will print following output:

```
------Collecting Anomaly Detection Results------
-------------------------------------
------Collecting Traffic Classification Results------
-------------------------------------
------Collecting Botnet Detection Results------
-------------------------------------
Applications                         F1 Score    CUs    MUs
---------------------------------  ----------  -----  -----
Baseline Anomaly Detection            71.1        24     48
Homunculus Anomaly Detection          83.10       41     67
Baseline Traffic Classification       61.04       31     59
Homunculus Traffic Classification     68.75       54     97
Baseline Botnet Detection             77.04      167     45
Homunculus Botnet Detection           79.80       53     151
```

- If you want to collect the previously generated results and print them again, you can run just the collection script. 
```
./collect-artifact-results.sh
```

## Contact Us 
- [Tushar Swamy](https://www.linkedin.com/in/tushar-swamy-b4aa51b1/)
- [Annus Zulfiqar](https://annusgit.github.io/)
- [Muhammad Shahbaz](https://mshahbaz.gitlab.io/)
- [Kunle Olukotun](http://arsenalfc.stanford.edu/kunle/)
